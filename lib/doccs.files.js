const config = require('../../../.doccsrc.js');
const copydir = require('copy-dir');
const fs = require('fs');
const path = require('path');
const rimraf = require('rimraf');

const ensureDirectoryExists = (directory) => {
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory);
  }
};

const performFileSystemActions = () => {
  rimraf.sync('./src/js/articles');
  rimraf.sync(path.join(__dirname, '../../../', config.output));

  copydir.sync(path.join(__dirname, '../../../', config.docs), './src/js/articles');

  if (config.site && config.site.componentStyles) {
    const assetsDirectory = path.join(__dirname, '../../../', config.output, 'assets/');
    const stylesheetDirectory = path.join(assetsDirectory, 'css/');

    ensureDirectoryExists(path.join(__dirname, '../../../', config.output));
    ensureDirectoryExists(assetsDirectory);
    ensureDirectoryExists(stylesheetDirectory);

    const stylesheetFile = path.join(__dirname, '../../../', config.site.componentStyles);
    const stylesheetData = fs.readFileSync(stylesheetFile).toString();

    fs.writeFileSync(path.join(stylesheetDirectory, 'components.min.css'), stylesheetData, { flag: 'w+' });
  }
};

module.exports = performFileSystemActions;
