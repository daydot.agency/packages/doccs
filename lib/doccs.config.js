const config = require('../../../.doccsrc.js');
const fs = require('fs');
const path = require('path');

const generateStaticConfig = (dest) => {
  const staticConfig = Object.assign(config, {});

  if (staticConfig.webpack) {
    delete staticConfig.webpack;
  }

  fs.writeFileSync(path.join(dest, 'doccsrc.json'), JSON.stringify(staticConfig, null, 2), { flag: 'w+' });
};

module.exports = generateStaticConfig;
