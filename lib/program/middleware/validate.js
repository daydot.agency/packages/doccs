const OutputService = require('../services/OutputService');
const config = require('../../../../../.doccsrc.js');
const process = require('process');

const validate = (next) => {
  if (!config.docs) {
    OutputService.log('Your .doccsrc.js file is missing the "docs" key'.red);
    process.exit();
  }

  if (!config.output) {
    OutputService.log('Your .doccsrc.js file is missing the "output" key'.red);
    process.exit();
  }

  next();
};

module.exports = validate;
