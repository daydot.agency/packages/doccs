class MiddlewareService {
  call(next) {
    next();
  }

  use(callback) {
    this.call = ((stack) => (next) => stack(callback.bind(this, next.bind(this))))(this.call);
  }
}

module.exports = MiddlewareService;
