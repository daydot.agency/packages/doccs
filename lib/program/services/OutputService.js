const moment = require('moment');

class OutputService {
  static log(message) {
    console.log(`[${moment().format('H:mm:ss')}]`, message);
  }
}

module.exports = OutputService;
