const exec = require('child_process').exec;

const build = () => {
  const execBuildProcess = exec('cd ./node_modules/doccs && NODE_ENV=production npm run build');

  execBuildProcess.stdout.on('data', (data) => process.stdout.write(data));
};

module.exports = {
  build
};
