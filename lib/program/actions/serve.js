const exec = require('child_process').exec;

const serve = (options) => {
  const execServeProcess = exec(`cd ./node_modules/doccs && NODE_ENV=development APP_PORT=${options.port} npm run serve`);

  execServeProcess.stdout.on('data', (data) => process.stdout.write(data));
};

module.exports = {
  serve
};
