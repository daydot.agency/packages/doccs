const MiddlewareService = require('./services/MiddlewareService');
const program = require('commander');
const validate = require('./middleware/validate');
const { build } = require('./actions/build');
const { serve } = require('./actions/serve');

// Middlware
// ------------------------------

const middleware = new MiddlewareService();

middleware.use((next) => validate(next));

// Program
// ------------------------------

program
  .version('1.0.0')
  .description('Doccs');

// Build
// ------------------------------

program
  .command('build')
  .action(() => middleware.call(() => build()));

// Serve
// ------------------------------

program
  .command('serve')
  .option('-p, --port <type>', 'the port to serve the app on', '8080')
  .action((options) => middleware.call(() => serve(options)));

module.exports = program;
