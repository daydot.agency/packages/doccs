const config = require('../../../.doccsrc.js');
const fs = require('fs');
const matter = require('gray-matter');
const path = require('path');

const recursivelyGetFileData = (docs, callback) => {
  const files = fs.readdirSync(docs);

  files.map((file) => {
    if (fs.lstatSync(path.join(docs, file)).isDirectory()) {
      return recursivelyGetFileData(path.join(docs, file), callback);
    }

    callback(fs.readFileSync(path.join(docs, file)).toString());
  });
};

const generateDocumentationSiteMap = (dest) => {
  const sitemap = {};

  recursivelyGetFileData(path.join(__dirname, '../../../', config.docs), (content) => {
    const { data } = matter(content);
    const section = undefined !== data.section ? data.section.toLowerCase() : '0';
    const name = data.name.replace(' ', '-');

    sitemap[section] = undefined !== sitemap[section] ? sitemap[section] : [];

    sitemap[section].push({
      name: data.name.toLowerCase(),
      path: '0' === section ? `/${name}` : `/${section}/${name}`
    });
  });

  fs.writeFileSync(path.join(dest, 'sitemap.json'), JSON.stringify(sitemap, null, 2), { flag: 'w+' });
};

module.exports = generateDocumentationSiteMap;
