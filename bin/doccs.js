#!/usr/bin/env node

require('colors');

const OutputService = require('../lib/program/services/OutputService');
const fs = require('fs');

if (!fs.existsSync('./.doccsrc.js')) {
  OutputService.log('Looks like you don\'t have a valid .doccsrc.js file'.red);
  OutputService.log('Please ensure that you have created one'.red);
  process.exit();
}

const program = require('../lib/program');

program.parse(process.argv);
