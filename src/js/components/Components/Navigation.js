import PropTypes from 'prop-types';
import sitemap from '@doccs/src/sitemap.json';
import { actions } from '@doccs/src/actions';
import { connect } from 'unistore/preact';
import { decorate } from '@doccs/src/support/helpers/decorate';
import { h } from 'preact';
import { route } from 'preact-router';
import { default as titleize } from 'underscore.string/titleize';

export const Navigation = (props) => {
  const { isSidebarVisible, query, setSidebarVisibility } = props;

  return (
    <div className={decorate('c-navigation')}>
      {
        Object.entries(sitemap).map(([section, entries]) => {
          if ('' !== query) {
            if (!entries.some((entry) => -1 !== entry.name.indexOf(query.toLowerCase()))) {
              return null;
            }
          }

          return (
            <div key={section} className={decorate('c-navigation__section')}>
              {
                '0' !== section &&
                <div className={decorate('c-navigation__sectionHeading')}>{titleize(section)}</div>
              }
              <div className={decorate('c-navigation__sectionArticles')}>
                {
                  entries.map((entry) => {
                    if (-1 === entry.name.indexOf(query.toLowerCase())) {
                      return null;
                    }

                    return (
                      <div key={entry.name} className={decorate('c-navigation__sectionArticle')} role='button' onClick={() => (route(entry.path), isSidebarVisible && setSidebarVisibility(false))}>
                        {titleize(entry.name)}
                      </div>
                    );
                  })
                }
              </div>
            </div>
          );
        })
      }
    </div>
  );
};

Navigation.propTypes = {
  isSidebarVisible: PropTypes.bool,
  query: PropTypes.string.isRequired,
  setSidebarVisibility: PropTypes.func
};

const mapStateToProps = (state) => {
  return {
    isSidebarVisible: state.isSidebarVisible
  };
};

const mapDispatchToProp = () => {
  return {
    setSidebarVisibility: actions.setSidebarVisibility
  };
};

export default connect(mapStateToProps, mapDispatchToProp)(Navigation);
