import PropTypes from 'prop-types';
import RepositoryDarkIcon from '@doccs/src/svg/repository--dark.svg';
import RepositoryLightIcon from '@doccs/src/svg/repository--light.svg';
import config from '@doccs/src/doccsrc.json';
import root from 'window-or-global';
import { connect } from 'unistore/preact';
import { decorate } from '@doccs/src/support/helpers/decorate';
import { h } from 'preact';

export const RepositoryLink = (props) => {
  const { currentUiTheme } = props;

  return (
    <div className={decorate('c-repositoryLink')} role='button' onClick={() => root.open(config.repository, '_blank', 'noopener noreferrer')}>
      {'dark' === currentUiTheme ? <RepositoryDarkIcon height='15px' width='15px' /> : <RepositoryLightIcon height='15px' width='15px' />}
    </div>
  );
};

RepositoryLink.propTypes = {
  currentUiTheme: PropTypes.string
};

const mapStateToProps = (state) => {
  return {
    currentUiTheme: state.currentUiTheme
  };
};

export default connect(mapStateToProps, {})(RepositoryLink);
