import PropTypes from 'prop-types';
import SunDarkIcon from '@doccs/src/svg/sun--dark.svg';
import SunLightIcon from '@doccs/src/svg/sun--light.svg';
import { actions } from '@doccs/src/actions';
import { connect } from 'unistore/preact';
import { decorate } from '@doccs/src/support/helpers/decorate';
import { h } from 'preact';
import { setStorage } from '@doccs/src/support/helpers/storage';

export const ThemeToggle = (props) => {
  const { currentUiTheme, setCurrentUiTheme } = props;
  const alternateThemeState = 'dark' === currentUiTheme ? 'light' : 'dark';

  return (
    <div className={decorate('c-themeToggle')} role='button' onClick={() => (setCurrentUiTheme(alternateThemeState), setStorage('theme', alternateThemeState))}>
      {'dark' === currentUiTheme ? <SunDarkIcon height='17px' width='17px' /> : <SunLightIcon height='17px' width='17px' />}
    </div>
  );
};

ThemeToggle.propTypes = {
  currentUiTheme: PropTypes.string,
  setCurrentUiTheme: PropTypes.func
};

const mapStateToProps = (state) => {
  return {
    currentUiTheme: state.currentUiTheme
  };
};

const mapDispatchToProps = () => {
  return {
    setCurrentUiTheme: actions.setCurrentUiTheme
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ThemeToggle);
