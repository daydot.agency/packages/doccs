import PropTypes from 'prop-types';
import SearchbarIcon from '@doccs/src/svg/search.svg';
import { decorate } from '@doccs/src/support/helpers/decorate';
import { h } from 'preact';

export const Searchbar = (props) => {
  const { onSearch } = props;

  return (
    <div className={decorate('c-searchbar')}>
      <div className={decorate('c-searchbar__icon')}>
        <SearchbarIcon height='15px' width='15px' />
      </div>
      <input className={decorate('c-searchbar__input')} placeholder='Type to search...' type='text' onKeyUp={(event) => onSearch(event.target.value)} />
    </div>
  );
};

Searchbar.propTypes = {
  onSearch: PropTypes.func.isRequired
};

export default Searchbar;
