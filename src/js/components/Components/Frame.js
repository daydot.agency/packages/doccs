import FrameContent from '@doccs/src/components/Components/FrameContent';
import PropTypes from 'prop-types';
import { Component, createRef, h } from 'preact';
import { createPortal } from 'preact/compat';

export class Frame extends Component {
  constructor() {
    super();

    this.state = {
      frameHeight: 0,
      isMounted: false
    };

    this.frame = createRef();
  }

  componentDidMount() {
    this.setState({ isMounted: true });

    if (this.frame && 'complete' === this.frame.current.readState) {
      this.onLoad();
    } else {
      this.frame.current.addEventListener('load', this.onLoad);
    }
  }

  componentWillUnmount() {
    this.setState({ isMounted: false });
    this.frame.current.removeEventListener('load', this.onLoad);
  }

  getFrameContent = () => {
    const { children } = this.props;

    return (
      <FrameContent contentDidMount={this.onHandleFrameResize} contentDidUpdate={this.onHandleFrameResize}>
        <div className='frame-content'>{children}</div>
      </FrameContent>
    );
  }

  getMountableTarget = () => {
    return this.frame.current.contentDocument.body.children[0];
  }

  getRenderableContent = () => {
    const { isMounted } = this.state;

    if (!isMounted || !this.frame.current) {
      return null;
    }

    this.ensureFrameHasInitialContent();
    this.frame.current.contentDocument.body.style.margin = 0;

    return [
      createPortal(this.getFrameContent(), this.getMountableTarget())
    ];
  }

  onHandleFrameResize = () => {
    const { frameHeight } = this.state;
    const height = this.frame.current.contentDocument.body.offsetHeight ?? 0;

    if (frameHeight !== height) {
      this.setState({ frameHeight: height });
    }
  }

  ensureFrameHasInitialContent = () => {
    const { head, initialContent } = this.props;

    if (0 !== this.frame.current.contentDocument.body.children.length) {
      return;
    }

    const preppedInitialContent = initialContent.replace('<head></head>', `<head>${head.join('')}</head>`);

    this.frame.current.contentDocument.open('text/html', 'replace');
    this.frame.current.contentDocument.write(preppedInitialContent);
    this.frame.current.contentDocument.close();
  }

  onLoad = () => {
    this.forceUpdate();
  }

  render() {
    const { frameHeight } = this.state;

    return (
      <iframe ref={this.frame} style={{ height: frameHeight }} title='component iframe'>
        {this.getRenderableContent()}
      </iframe>
    );
  }
}

Frame.defaultProps = {
  head: [],
  initialContent: '<!DOCTYPE html><html><head></head><body><div class="frame-root"></div></body></html>'
};

Frame.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]),
  head: PropTypes.arrayOf(PropTypes.string),
  initialContent: PropTypes.string
};

export default Frame;
