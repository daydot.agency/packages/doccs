import PropTypes from 'prop-types';
import { Component } from 'preact';

export class FrameContent extends Component {
  componentDidMount() {
    const { contentDidMount } = this.props;

    contentDidMount();
  }

  componentDidUpdate() {
    const { contentDidUpdate } = this.props;

    contentDidUpdate();
  }

  render() {
    const { children } = this.props;

    return children;
  }
}

FrameContent.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]),
  contentDidMount: PropTypes.func.isRequired,
  contentDidUpdate: PropTypes.func.isRequired
};

export default FrameContent;
