import BurgerDarkIcon from '@doccs/src/svg/burger--dark.svg';
import BurgerLightIcon from '@doccs/src/svg/burger--light.svg';
import PropTypes from 'prop-types';
import { actions } from '@doccs/src/actions';
import { connect } from 'unistore/preact';
import { decorate } from '@doccs/src/support/helpers/decorate';
import { h } from 'preact';

export const BurgerToggle = (props) => {
  const { currentUiTheme, isSidebarVisible, setSidebarVisibility } = props;

  return (
    <div className={decorate('c-burgerToggle')} role='button' onClick={() => setSidebarVisibility(!isSidebarVisible)}>
      {'dark' === currentUiTheme ? <BurgerDarkIcon height='13px' width='13px' /> : <BurgerLightIcon height='13px' width='13px' />}
    </div>
  );
};

BurgerToggle.propTypes = {
  currentUiTheme: PropTypes.string,
  isSidebarVisible: PropTypes.bool,
  setSidebarVisibility: PropTypes.func
};

const mapStateToProps = (state) => {
  return {
    currentUiTheme: state.currentUiTheme,
    isSidebarVisible: state.isSidebarVisible
  };
};

const mapDispatchToProps = () => {
  return {
    setSidebarVisibility: actions.setSidebarVisibility
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BurgerToggle);
