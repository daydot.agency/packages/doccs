import PropTypes from 'prop-types';
import { h } from 'preact';
import { useEffect, useState } from 'preact/hooks';

export const MDXArticleRenderRoute = (props) => {
  const { path } = props;
  const [component, setComponent] = useState({});

  useEffect(async () => {
    const article = await import(`@doccs/src/articles/${path.replace(/^\//, '')}.mdx`);

    setComponent({ ...component, article: article.default });
  }, [props.path]);

  if (undefined === component.article) {
    return null;
  }

  return <component.article />;
};

MDXArticleRenderRoute.propTypes = {
  path: PropTypes.string
};

export default MDXArticleRenderRoute;
