import PropTypes from 'prop-types';

export const ConditionalComponent = (props) => {
  const { children, expression } = props;

  if (!expression) {
    return null;
  }

  return children;
};

ConditionalComponent.propTypes = {
  children: PropTypes.oneOfType([PropTypes.any, PropTypes.arrayOf(PropTypes.any)]).isRequired,
  expression: PropTypes.any.isRequired
};

export default ConditionalComponent;
