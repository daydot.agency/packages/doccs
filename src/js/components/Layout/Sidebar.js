import Navigation from '@doccs/src/components/Components/Navigation';
import PropTypes from 'prop-types';
import Searchbar from '@doccs/src/components/Components/Searchbar';
import classNames from 'classnames';
import { actions } from '@doccs/src/actions';
import { connect } from 'unistore/preact';
import { decorate } from '@doccs/src/support/helpers/decorate';
import { h } from 'preact';
import { useState } from 'preact/hooks';

export const Sidebar = (props) => {
  const { isSidebarVisible, setSidebarVisibility } = props;
  const [search, setSearch] = useState('');

  return (
    <div className={classNames(decorate('c-sidebar'), { [`${decorate('c-sidebar--visible')}`]: isSidebarVisible })} role='button' onClick={() => isSidebarVisible && setSidebarVisibility(false)}>
      <div className={decorate('c-sidebar__column')} role='button' onClick={(event) => event.stopPropagation()}>
        <Searchbar onSearch={setSearch} />
        <Navigation query={search} />
      </div>
    </div>
  );
};

Sidebar.propTypes = {
  isSidebarVisible: PropTypes.bool,
  setSidebarVisibility: PropTypes.func
};

const mapStateToProps = (state) => {
  return {
    isSidebarVisible: state.isSidebarVisible
  };
};

const mapDispatchToProp = () => {
  return {
    setSidebarVisibility: actions.setSidebarVisibility
  };
};

export default connect(mapStateToProps, mapDispatchToProp)(Sidebar);
