import BurgerToggle from '@doccs/src/components/Components/BurgerToggle';
import ConditionalComponent from '@doccs/src/components/ConditionalComponent';
import RepositoryLink from '@doccs/src/components/Components/RepositoryLink';
import ResponsiveComponent from '@doccs/src/components/ResponsiveComponent';
import ThemeToggle from '@doccs/src/components/Components/ThemeToggle';
import config from '@doccs/src/doccsrc.json';
import { decorate } from '@doccs/src/support/helpers/decorate';
import { h } from 'preact';

export const Masthead = () => {
  return (
    <div className={decorate('c-masthead')}>
      <div className={decorate('c-masthead__headline')}>
        {config.site && config.site.title ? config.site.title : 'My doccs'}
      </div>
      <div className={decorate('c-masthead__toggle')}>
        <ThemeToggle />
        <ConditionalComponent children={<RepositoryLink />} expression={undefined !== config.repository} />
        <ResponsiveComponent children={<BurgerToggle />} display={['mobile']} />
      </div>
    </div>
  );
};

export default Masthead;
