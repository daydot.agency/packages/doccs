import MDXArticleRenderRoute from '@doccs/src/components/Routes/MDXArticleRenderRoute';
import Router from 'preact-router';
import sitemap from '@doccs/src/sitemap.json';
import { createHashHistory } from 'history';
import { decorate } from '@doccs/src/support/helpers/decorate';
import { h } from 'preact';
import { useState } from 'preact/hooks';

export const Article = () => {
  const [url, setUrl] = useState(null);

  return (
    <div className={decorate('c-article')}>
      <Router history={createHashHistory()} url={url} onChange={setUrl}>
        {
          Object.entries(sitemap).map(([section, entries], sectionIndex) => { // eslint-disable-line no-unused-vars
            return entries.map((entry, entryIndex) => {
              return <MDXArticleRenderRoute key={entry.path} default={[entryIndex, sectionIndex].every((index) => 0 === index)} path={entry.path} />;
            });
          })
        }
      </Router>
    </div>
  );
};

export default Article;
