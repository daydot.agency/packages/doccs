import PropTypes from 'prop-types';
import { Component } from 'preact';
import { connect } from 'unistore/preact';

export class ResponsiveComponent extends Component {
  shouldRender = () => {
    const { currentDeviceString, display } = this.props;

    return display.some((breakpoint) => breakpoint === currentDeviceString);
  }

  render() {
    const { children } = this.props;

    if (!this.shouldRender()) {
      return null;
    }

    return children;
  }
}

ResponsiveComponent.propTypes = {
  children: PropTypes.oneOfType([PropTypes.any, PropTypes.arrayOf(PropTypes.any)]).isRequired,
  currentDeviceString: PropTypes.string,
  display: PropTypes.arrayOf(PropTypes.string).isRequired
};

const mapStateToProps = (state) => {
  return {
    currentDeviceString: state.currentDeviceString
  };
};

export default connect(mapStateToProps, {})(ResponsiveComponent);
