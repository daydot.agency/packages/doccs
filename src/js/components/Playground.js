import ClipboardDarkIcon from '@doccs/src/svg/clipboard--dark.svg';
import ClipboardLightIcon from '@doccs/src/svg/clipboard--light.svg';
import CodingDarkIcon from '@doccs/src/svg/coding--dark.svg';
import CodingLightIcon from '@doccs/src/svg/coding--light.svg';
import ConditionalComponent from '@doccs/src/components/ConditionalComponent';
import EditorDarkTheme from 'prism-react-renderer/themes/nightOwl';
import EditorLightTheme from 'prism-react-renderer/themes/github';
import Frame from '@doccs/src/components/Components/Frame';
import PropTypes from 'prop-types';
import SunDarkIcon from '@doccs/src/svg/sun--dark.svg';
import SunLightIcon from '@doccs/src/svg/sun--light.svg';
import classNames from 'classnames';
import config from '@doccs/src/doccsrc.json';
import root from 'window-or-global';
import { Component, createRef, h } from 'preact';
import { LiveEditor, LivePreview, LiveProvider } from 'react-live';
import { connect } from 'unistore/preact';
import { debounce } from '@doccs/src/support/helpers/debounce';
import { decorate } from '@doccs/src/support/helpers/decorate';

export class Playground extends Component {
  constructor(props) {
    super(props);

    const { currentUiTheme, showEditor } = this.props;

    this.state = {
      isEditorVisible: showEditor,
      isMounted: false,
      isResizing: false,
      localTheme: currentUiTheme,
      width: null
    };

    this.editor = createRef();
    this.preview = createRef();
    this.slider = createRef();
  }

  componentDidMount() {
    root.addEventListener('mousedown', this.onMouseDown);
    root.addEventListener('mousemove', this.onMouseMove);
    root.addEventListener('mouseup', this.onMouseUp);

    this.setState({ isMounted: true });
  }

  componentWillReceiveProps(nextProps) {
    const { localTheme } = this.state;

    if (nextProps.currentUiTheme !== localTheme) {
      this.setState({ localTheme: nextProps.currentUiTheme });
    }
  }

  componentWillUnmount() {
    root.removeEventListener('mousedown', this.onMouseDown);
    root.removeEventListener('mousemove', this.onMouseMove);
    root.removeEventListener('mouseup', this.onMouseUp);

    this.setState({ isMounted: false });
  }

  handleCopyToClipboard = () => {
    const textarea = this.editor.current.querySelector('textarea');

    textarea.select();
    document.execCommand('copy');
  }

  setEditorVisibility = (visibility) => {
    this.setState({ isEditorVisible: visibility });
  }

  setLocalTheme = (theme) => {
    this.setState({ localTheme: theme });
  }

  onMouseDown = (event) => {
    if (null !== this.slider.current && event.target === this.slider.current) {
      this.setState({ isResizing: true });
    }
  }

  onMouseMove = (event) => {
    debounce(() => {
      const { isResizing } = this.state;
      const calculatedWidth = event.clientX - this.preview.current.offsetLeft;

      if (isResizing && 200 < calculatedWidth) {
        this.setState({ width: event.clientX - this.preview.current.offsetLeft });
      }
    });
  }

  onMouseUp = () => {
    const { isMounted } = this.state;

    if (isMounted) {
      this.setState({ isResizing: false });
    }
  }

  constructPreviewHead = () => {
    const head = ['<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">'];

    if (config.site && config.site.componentStyles) {
      head.push('<link rel="stylesheet" href="/assets/css/components.min.css" />');
    }

    return head;
  }

  render() {
    const { currentUiTheme } = this.props;
    const { isEditorVisible, isMounted, isResizing, localTheme, width } = this.state;

    if (!isMounted) {
      return null;
    }

    return (
      <div className={decorate('c-playground')}>
        <LiveProvider {...this.props}>
          <div ref={this.preview} className={classNames(decorate('c-playground__preview'), decorate(`c-playground__preview--theme-${localTheme}`), { [`${decorate('c-playground__preview--noSelect')}`]: isResizing })} style={{ width }}>
            <div className={decorate('c-playground__previewThemeToggle')} role='button' onClick={() => this.setLocalTheme('dark' === localTheme ? 'light' : 'dark')}>
              {'dark' === localTheme ? <SunDarkIcon height='12px' width='12px' /> : <SunLightIcon height='12px' width='12px' />}
            </div>
            <div ref={this.slider} className={decorate('c-playground__previewWidthSlider')} />
            <Frame head={this.constructPreviewHead()}>
              <LivePreview />
            </Frame>
          </div>
          <div ref={this.editor} className={decorate('c-playground__editor')}>
            <div className={decorate('c-playground__editorClipboard')} role='button' onClick={this.handleCopyToClipboard}>
              {'dark' === currentUiTheme ? <ClipboardDarkIcon height='10px' width='10px' /> : <ClipboardLightIcon height='10px' width='10px' />}
            </div>
            <div className={decorate('c-playground__editorToggle')} role='button' onClick={() => this.setEditorVisibility(!isEditorVisible)}>
              {'dark' === currentUiTheme ? <CodingDarkIcon height='12px' width='12px' /> : <CodingLightIcon height='12px' width='12px' />}
            </div>
            <ConditionalComponent expression={isEditorVisible}>
              <div className={decorate('c-playground__editorCodeBox')}>
                <LiveEditor theme={'dark' === currentUiTheme ? EditorDarkTheme : EditorLightTheme} disabled />
              </div>
            </ConditionalComponent>
          </div>
        </LiveProvider>
      </div>
    );
  }
}

Playground.defaultProps = {
  showEditor: true
};

Playground.propTypes = {
  code: PropTypes.string.isRequired,
  currentUiTheme: PropTypes.string,
  scope: PropTypes.object.isRequired,
  showEditor: PropTypes.bool
};

const mapStateToProps = (state) => {
  return {
    currentUiTheme: state.currentUiTheme
  };
};

export default connect(mapStateToProps, {})(Playground);
