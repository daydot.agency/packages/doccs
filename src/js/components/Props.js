import PropTypes from 'prop-types';
import ResponsiveComponent from '@doccs/src/components/ResponsiveComponent';
import classNames from 'classnames';
import parsePropTypes from 'parse-prop-types';
import { Fragment, h } from 'preact';
import { decorate } from '@doccs/src/support/helpers/decorate';

export const Props = (props) => {
  const { of } = props;
  const parsedProps = parsePropTypes(of);

  return (
    <div className={decorate('c-props')}>
      <ResponsiveComponent display={['desktop']}>
        <div className={decorate('c-props__tableHeaders')}>
          <div className={classNames(decorate('c-props__tableHeader'), decorate('c-props--width-75'))}>Prop name</div>
          <div className={classNames(decorate('c-props__tableHeader'), decorate('c-props--width-100'))}>Type</div>
          <div className={classNames(decorate('c-props__tableHeader'), decorate('c-props--width-50'))}>Default value</div>
          <div className={classNames(decorate('c-props__tableHeader'), decorate('c-props--width-25'))}>Required</div>
        </div>
      </ResponsiveComponent>
      <div className={decorate('c-props__tableEntries')}>
        {
          Object.entries(parsedProps).map(([prop, details]) => {
            return (
              <div key={prop} className={decorate('c-props__tableEntry')}>
                <div className={classNames(decorate('c-props__tableEntryColumn'), decorate('c-props--width-75'))}>
                  <ResponsiveComponent display={['mobile']}>
                    <div className={decorate('c-props__tableHeader')}>Prop name</div>
                  </ResponsiveComponent>
                  <div className={classNames(decorate('c-props__tableEntryValue'), decorate('c-props__tableEntryValue--highlight'))}>
                    {prop}
                  </div>
                </div>

                <div className={classNames(decorate('c-props__tableEntryColumn'), decorate('c-props--width-100'))}>
                  <ResponsiveComponent display={['mobile']}>
                    <div className={decorate('c-props__tableHeader')}>Type</div>
                  </ResponsiveComponent>
                  <>
                    {
                      details.type.value && 'arrayOf' === details.type.name &&
                      <div className={classNames(decorate('c-props__tableEntryValue'), decorate('c-props__tableEntryValue--mono'))}>
                        array
                      </div>
                    }
                    {
                      details.type.value && 'oneOf' === details.type.name &&
                      details.type.value.map((entry, index) => {
                        return (
                          <div key={index} className={classNames(decorate('c-props__tableEntryValue'), decorate('c-props__tableEntryValue--mono'))}>
                            {`${JSON.stringify(entry)}`}
                          </div>
                        );
                      })
                    }
                    {
                      details.type.value && 'oneOfType' === details.type.name &&
                      <div className={classNames(decorate('c-props__tableEntryValue'), decorate('c-props__tableEntryValue--mono'))}>
                        mixed
                      </div>
                    }
                    {
                      !details.type.value &&
                      <div className={classNames(decorate('c-props__tableEntryValue'), decorate('c-props__tableEntryValue--mono'))}>
                        {details.type.name}
                      </div>
                    }
                  </>
                </div>

                <div className={classNames(decorate('c-props__tableEntryColumn'), decorate('c-props--width-50'))}>
                  <ResponsiveComponent display={['mobile']}>
                    <div className={decorate('c-props__tableHeader')}>Default value</div>
                  </ResponsiveComponent>
                  <div className={decorate('c-props__tableEntryValue')}>
                    {details.defaultValue ? JSON.stringify(details.defaultValue.value) : '—'}
                  </div>
                </div>

                <div className={classNames(decorate('c-props__tableEntryColumn'), decorate('c-props--width-25'))}>
                  <ResponsiveComponent display={['mobile']}>
                    <div className={decorate('c-props__tableHeader')}>Required</div>
                  </ResponsiveComponent>
                  <div className={classNames(decorate('c-props__tableEntryValue'), { [`${decorate('c-props__tableEntryValue--mono')}`]: details.required })}>
                    {details.required ? 'true' : '—'}
                  </div>
                </div>
              </div>
            );
          })
        }
      </div>
    </div>
  );
};

Props.propTypes = {
  of: PropTypes.func.isRequired
};

export default Props;
