export function setCurrentUiTheme(state, theme) {
  return {
    currentUiTheme: theme
  };
}
