import * as metaActions from '@doccs/src/actions/meta';
import * as themeActions from '@doccs/src/actions/theme';
import * as uiActions from '@doccs/src/actions/ui';

export const actions = Object.assign({},
  metaActions,
  themeActions,
  uiActions
);
