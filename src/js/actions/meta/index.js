export function setCurrentDeviceString(state, device) {
  return {
    currentDeviceString: device
  };
}

export function setCurrentWidowWidth(state, width) {
  return {
    currentWindowWidth: width
  };
}
