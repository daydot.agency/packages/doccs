export function setSidebarVisibility(state, visibility) {
  return {
    isSidebarVisible: visibility
  };
}
