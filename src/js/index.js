import 'parse-prop-types';

import Application from '@doccs/src/application';
import { Provider } from 'unistore/preact';
import { h, render } from 'preact';
import { store } from '@doccs/src/store';

import '../scss/index.scss';

if ('development' === process.env.NODE_ENV) {
  require('preact/debug');
}

render(
  <Provider store={store}>
    <Application />
  </Provider>,
  document.getElementById('docs')
);
