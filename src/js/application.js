import Article from '@doccs/src/components/Layout/Article';
import Masthead from '@doccs/src/components/Layout/Masthead';
import PropTypes from 'prop-types';
import Sidebar from '@doccs/src/components/Layout/Sidebar';
import classNames from 'classnames';
import root from 'window-or-global';
import { Component, Fragment, createRef, h } from 'preact';
import { actions } from '@doccs/src/actions';
import { connect } from 'unistore/preact';
import { debounce } from '@doccs/src/support/helpers/debounce';
import { decorate } from '@doccs/src/support/helpers/decorate';
import { getWindowSize } from '@doccs/src/support/helpers/meta';

export class Application extends Component {
  constructor() {
    super();

    this.resizeTimer = null;
    this.body = createRef();
  }

  componentWillMount() {
    const { setCurrentDeviceString, setCurrentWidowWidth } = this.props;
    const width = getWindowSize();
    const deviceString = this.getDeviceStringByWidth(width);

    setCurrentDeviceString(deviceString);
    setCurrentWidowWidth(width);
  }

  componentDidMount() {
    root.addEventListener('resize', this.handleResize, false);
  }

  componentWillUnmount() {
    root.removeEventListener('resize', this.handleResize, false);
  }

  handleResize = () => {
    debounce(() => {
      const { currentDeviceString, setCurrentDeviceString, setCurrentWidowWidth } = this.props;
      const width = getWindowSize();
      const deviceString = this.getDeviceStringByWidth(width);

      setCurrentWidowWidth(width);

      if (currentDeviceString !== deviceString) {
        setCurrentDeviceString(deviceString);
      }

      this.body.current.classList.add(decorate('u-animation--stop'));
      clearTimeout(this.resizeTimer);

      this.resizeTimer = setTimeout(() => {
        this.body.current.classList.remove(decorate('u-animation--stop'));
      }, 400);
    });
  }

  getDeviceStringByWidth = (width) => {
    return 991 > width ? 'mobile' : 'desktop';
  }

  render() {
    const { currentUiTheme } = this.props;

    return (
      <div ref={this.body} className={classNames(decorate('u-theme'), decorate(`u-theme--${currentUiTheme}`))}>
        <Masthead />
        <div className={decorate('c-body')}>
          <Sidebar />
          <Article />
        </div>
      </div>
    );
  }
}

Application.propTypes = {
  currentDeviceString: PropTypes.string,
  currentUiTheme: PropTypes.string,
  setCurrentDeviceString: PropTypes.func,
  setCurrentWidowWidth: PropTypes.func
};

const mapStateToProps = (state) => {
  return {
    currentDeviceString: state.currentDeviceString,
    currentUiTheme: state.currentUiTheme
  };
};

const mapDispatchToProps = () => {
  return {
    setCurrentDeviceString: actions.setCurrentDeviceString,
    setCurrentWidowWidth: actions.setCurrentWidowWidth
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Application);
