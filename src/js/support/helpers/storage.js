import root from 'window-or-global';

export function getStorage(key) {
  return root.localStorage.getItem(`dcs.${key}`);
}

export function setStorage(key, value) {
  return root.localStorage.setItem(`dcs.${key}`, value);
}
