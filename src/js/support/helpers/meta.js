import root from 'window-or-global';

export function getWindowSize() {
  return root.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
}
