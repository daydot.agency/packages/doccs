import root from 'window-or-global';

export function debounce(callback) {
  let isTicking = false;

  if (!isTicking) {
    root.requestAnimationFrame(() => callback(), isTicking = false);
    isTicking = true;
  }
}
