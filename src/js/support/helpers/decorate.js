export function decorate(selector) {
  if (null === selector) {
    return null;
  }

  return `dcs-${selector}`;
}
