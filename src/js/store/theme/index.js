import root from 'window-or-global';
import { getStorage } from '@doccs/src/support/helpers/storage';

const prefersDarkMode = root.matchMedia && root.matchMedia('(prefers-color-scheme: dark)').matches;
const storedTheme = getStorage('theme');

let initialTheme = 'light';

if (null === storedTheme && prefersDarkMode) {
  initialTheme = 'dark';
}

if (null !== storedTheme && initialTheme !== storedTheme) {
  initialTheme = storedTheme;
}

export const store = {
  currentUiTheme: initialTheme
};
