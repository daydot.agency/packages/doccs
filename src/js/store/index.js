import create from 'unistore';
import { store as meta } from '@doccs/src/store/meta';
import { store as theme } from '@doccs/src/store/theme';
import { store as ui } from '@doccs/src/store/ui';

const initialStore = Object.assign({},
  meta,
  theme,
  ui
);

const store = create(initialStore);

export { initialStore, store };
