const connect = require('gulp-connect');
const gulp = require('gulp');
const path = require('path');
const plumber = require('gulp-plumber');
const watch = require('gulp-watch');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');

//
// ----------------------------------------

const config = require('../../.doccsrc.js');
const webpackConfig = require('./webpack.config.js');
const transformedWebpackConfig = config.webpack ? config.webpack(webpackConfig) : webpackConfig;

const generateDocumentationSiteMap = require('./lib/doccs.pages');
const generateStaticConfig = require('./lib/doccs.config');
const performFileSystemActions = require('./lib/doccs.files');

// Compile
// ----------------------------------------

gulp.task('build', () => {
  generateDocumentationSiteMap('./src/js');
  generateStaticConfig('./src/js');
  performFileSystemActions();

  return gulp.src(['./src/js/index.js'])
    .pipe(plumber())
    .pipe(webpackStream(transformedWebpackConfig, webpack))
    .pipe(gulp.dest(path.join(__dirname, '../../', config.output)));
});

// Serve
// ----------------------------------------

gulp.task('serve', () => {
  connect.server({ livereload: false, port: process.env.APP_PORT, root: path.join(__dirname, '../../', config.output) });
  watch(['./src/**/*'].concat(config.watch ? config.watch : []), () => gulp.start('build'));
});
