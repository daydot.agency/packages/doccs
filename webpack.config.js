const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const config = require('../../.doccsrc.js');
const path = require('path');

const isProduction = 'production' === process.env.NODE_ENV;

module.exports = {
  devtool: !isProduction ? 'eval-cheap-source-map' : false,

  entry: {
    script: './src/js/index.js'
  },

  mode: isProduction ? 'production' : 'development',

  module: {
    rules: [
      {
        exclude: /node_modules\/(?!(doccs)\/).*/,
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader'
        ]
      },
      {
        exclude: /node_modules\/(?!(doccs)\/).*/,
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              plugins: [
                ['@babel/plugin-proposal-class-properties', { 'loose': false }],
                ['@babel/plugin-proposal-decorators', { 'legacy': true }],
                ['@babel/plugin-proposal-nullish-coalescing-operator'],
                ['@babel/plugin-transform-react-jsx', { 'pragma': 'h', 'pragmaFrag': 'Fragment' }],
                ['@babel/transform-runtime'],
                ['transform-commonjs-es2015-modules']
              ],
              presets: ['@babel/preset-env', '@babel/preset-react']
            }
          }
        ]
      },
      {
        exclude: /node_modules\/(?!(doccs)\/).*/,
        test: /\.mdx$/,
        use: [
          'babel-loader',
          '@mdx-js/loader',
          path.join(__dirname, './lib/loaders/fm-loader')
        ]
      },
      {
        exclude: /node_modules\/(?!(doccs)\/).*/,
        test: /\.svg$/,
        use: [
          'preact-svg-loader'
        ]
      }
    ]
  },

  optimization: {
    minimize: isProduction,
    minimizer: !isProduction ? [] : [
      new TerserPlugin({
        cache: true,
        extractComments: false,
        parallel: true
      })
    ],
    splitChunks: {
      cacheGroups: {
        commons: {
          chunks: 'all',
          name: 'vendor',
          test: /node_modules\/(?!(doccs|parse-prop-types)\/).*/
        }
      }
    }
  },

  output: {
    chunkFilename: 'assets/js/[chunkhash].min.js',
    filename: 'assets/js/[name].[chunkhash].min.js',
    path: path.join(__dirname, '../../', config.output),
    publicPath: '/'
  },

  performance: {
    hints: false
  },

  plugins: [
    new HtmlWebpackPlugin({
      minify: true,
      scriptLoading: 'defer',
      template: './src/html/index.html',
      title: config.site && config.site.title ? config.site.title : 'Generated documentation | Doccs',
      xhtml: true
    }),
    new MiniCssExtractPlugin({
      chunkFilename: 'assets/css/[chunkhash].min.css',
      filename: 'assets/css/style.[chunkhash].min.css'
    })
  ],

  resolve: {
    alias: Object.assign(
      {
        '@doccs/components': path.resolve(__dirname, 'src/', 'js/', 'components'),
        '@doccs/root': path.resolve(__dirname, '../../'),
        '@doccs/src': path.resolve(__dirname, 'src/', 'js'),
        'preact': path.resolve(__dirname, '../', 'preact'),
        'preact/hooks': path.resolve(__dirname, '../', 'preact', 'hooks'),
        'react': 'preact/compat',
        'react-dom': 'preact/compat'
      }
    ),
    extensions: ['.js', '.json']
  },

  stats: 'errors-only'
};
